package org.wikimedia.discovery.cirrus.updater.producer.graph;

public class InvalidMWApiResponseException extends RuntimeException {
    public InvalidMWApiResponseException(String s) {
        super(s);
    }
}
