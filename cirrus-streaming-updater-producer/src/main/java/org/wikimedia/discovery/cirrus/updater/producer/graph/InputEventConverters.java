package org.wikimedia.discovery.cirrus.updater.producer.graph;

import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import com.google.common.base.Preconditions;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
@SuppressFBWarnings(value = "CE_CLASS_ENVY", justification = "false positive")
public class InputEventConverters {

    private static final String PAGE_CHANGE_SCHEMA = "/development/mediawiki/page/change";

    public static InputEvent fromSource(Row row) {
        return fromPageChange(row);
    }

    public static InputEvent fromPageChange(Row row) {
        final String schema = row.getFieldAs("$schema");
        Preconditions.checkArgument(schema != null, "Undefined source schema");
        Preconditions.checkArgument(
                schema.startsWith(PAGE_CHANGE_SCHEMA), "Unknown source schema: " + schema);

        InputEvent event = new InputEvent();

        Row meta = row.getFieldAs("meta");
        event.setDomain(meta.getFieldAs("domain"));
        event.setRequestId(meta.getFieldAs("request_id"));
        event.setEventStream(meta.getFieldAs("stream"));

        String pageChangeKind = row.getFieldAs("page_change_kind");

        if ("delete".equals(pageChangeKind)) {
            event.setChangeType(ChangeType.PAGE_DELETE);
        } else {
            event.setChangeType(ChangeType.REV_BASED_UPDATE);
        }

        Row page = row.getFieldAs("page");
        event.setWikiId(row.getFieldAs("wiki_id"));
        event.setPageId(page.getFieldAs("page_id"));
        event.setPageTitle(page.getFieldAs("page_title"));
        event.setPageNamespace(page.getFieldAs("namespace_id"));

        Row revision = row.getFieldAs("revision");
        event.setRevId(revision.getFieldAs("rev_id"));
        event.setEventTime(revision.getFieldAs("rev_dt"));

        return event;
    }
}
