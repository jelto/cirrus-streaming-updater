package org.wikimedia.discovery.cirrus.updater.producer.sink;

import static org.wikimedia.discovery.cirrus.updater.producer.model.JsonPathUtils.getRequiredNode;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema.Builder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class UpdateRowEncoder implements Serializable {

    private static final long serialVersionUID = 7583439220125733556L;

    private final EventRowTypeInfo outputTypeInfo;
    private final JsonRowDeserializationSchema fieldsSchema;
    private final ObjectMapper objectMapper;
    private final ObjectWriter objectWriter;

    public UpdateRowEncoder(EventRowTypeInfo outputTypeInfo, ObjectMapper objectMapper) {
        this.outputTypeInfo = outputTypeInfo;

        this.fieldsSchema = new Builder(outputTypeInfo.getTypeAt("fields")).build();
        this.objectMapper = objectMapper;
        this.objectWriter = objectMapper.writer();
    }

    public EventRowTypeInfo getOutputTypeInfo() {
        return outputTypeInfo;
    }

    /**
     * Encodes {@code node} as {@link Row}. The {@link Row} schema is defined by {@link
     * #outputTypeInfo}.
     *
     * @param page {@link JsonNode} at {@code /query/pages/0} fetched from MediaWiki/CirrusSearch API
     * @param inputEvent input event
     * @return encoded row
     * @throws IOException if encoding the {@code fields} property fails
     * @throws IllegalArgumentException if {@code node} lacks an expected property
     */
    public Row encodeCirrusBuildDoc(JsonNode page, InputEvent inputEvent) throws IOException {
        Objects.requireNonNull(page, "node must not be null");
        Objects.requireNonNull(inputEvent, "inputEvent must not be null");

        final Row row = encodeInputEvent(inputEvent);

        final JsonNode cirrusBuildDoc = getRequiredNode(page, "/cirrusbuilddoc");
        final JsonNode cirrusBuildDocMetadata = getRequiredNode(page, "/cirrusbuilddoc_metadata");

        row.setField(UpdateFields.PAGE_ID, getRequiredNode(cirrusBuildDoc, "/page_id").asLong());
        row.setField(
                UpdateFields.PAGE_NAMESPACE, getRequiredNode(cirrusBuildDoc, "/namespace").asLong());
        row.setField(UpdateFields.REV_ID, getRequiredNode(cirrusBuildDoc, "/version").asLong());

        row.setField(
                "cluster_group", getRequiredNode(cirrusBuildDocMetadata, "/cluster_group").asText());
        if (cirrusBuildDocMetadata.has("index_name")) {
            row.setField(
                    UpdateFields.INDEX_NAME, getRequiredNode(cirrusBuildDocMetadata, "/index_name").asText());
        }

        final Map<String, String> noopHints =
                objectMapper.convertValue(
                        getRequiredNode(cirrusBuildDocMetadata, "/noop_hints"),
                        new TypeReference<Map<String, String>>() {});

        row.setField(UpdateFields.NOOP_HINTS, noopHints);

        final Row fields = fieldsSchema.deserialize(objectWriter.writeValueAsBytes(cirrusBuildDoc));

        row.setField(UpdateFields.FIELDS, fields);

        checkConsistency(inputEvent, row);

        return row;
    }

    @Nonnull
    public Row encodeInputEvent(InputEvent inputEvent) {
        final Row row = outputTypeInfo.createEmptyRow();

        row.setField(UpdateFields.OPERATION, asOperation(inputEvent));

        row.setField("dt", inputEvent.getEventTime());
        row.setField("domain", inputEvent.getDomain());
        row.setField("wiki_id", inputEvent.getWikiId());

        row.setField(UpdateFields.PAGE_ID, inputEvent.getPageId());
        row.setField(UpdateFields.PAGE_NAMESPACE, inputEvent.getPageNamespace());
        row.setField(UpdateFields.REV_ID, inputEvent.getRevId());

        return row;
    }

    private static String asOperation(InputEvent inputEvent) {
        switch (inputEvent.getChangeType()) {
            case PAGE_DELETE:
                return UpdateFields.OPERATION_DELETE;
            case REV_BASED_UPDATE:
                return UpdateFields.OPERATION_UPDATE_REVISION;
            default:
                throw new IllegalArgumentException(
                        "No operation mapping for page-change-type "
                                + inputEvent.getChangeType()
                                + " of page-id "
                                + inputEvent.getPageId()
                                + " in "
                                + inputEvent.getWikiId());
        }
    }

    /**
     * Check if input ({@code revisionCreate}) and output ({@code update}) are consistent. Compares
     * page ID and revision ID.
     *
     * @param inputEvent input
     * @param update output
     * @throws IllegalArgumentException if there is a mismatch
     */
    private static void checkConsistency(InputEvent inputEvent, Row update) {
        final Long outputPageId = UpdateFields.getPageId(update);
        if (!Objects.equals(inputEvent.getPageId(), outputPageId)) {
            throw new IllegalArgumentException(
                    "Page ID mismatch: input=" + inputEvent.getPageId() + " != output=" + outputPageId);
        }
        final Long outputRevisionId = UpdateFields.getRevId(update);
        if (!Objects.equals(inputEvent.getRevId(), outputRevisionId)) {
            throw new IllegalArgumentException(
                    "Revision ID mismatch: input="
                            + inputEvent.getRevId()
                            + " != output="
                            + outputRevisionId);
        }
    }
}
