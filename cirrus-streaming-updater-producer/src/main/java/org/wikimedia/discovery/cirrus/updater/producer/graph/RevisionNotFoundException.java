package org.wikimedia.discovery.cirrus.updater.producer.graph;

public class RevisionNotFoundException extends RuntimeException {
    public RevisionNotFoundException(String message) {
        super(message);
    }
}
