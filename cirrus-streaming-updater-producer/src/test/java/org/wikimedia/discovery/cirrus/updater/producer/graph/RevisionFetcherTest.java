package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.flink.types.Row;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mock.Strictness;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.testcontainers.shaded.com.google.common.collect.ImmutableMap;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.sink.UpdateRowEncoder;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;

@ExtendWith(MockitoExtension.class)
class RevisionFetcherTest {

    @RegisterExtension static WireMockExtension wireMockExtension = getWireMockExtension();

    static final String DOMAIN = "mydomain.local";

    RevisionFetcher revisionFetcher;

    @Mock(strictness = Strictness.LENIENT)
    UpdateRowEncoder encoder;

    @BeforeEach
    void createFetcher(WireMockRuntimeInfo info) throws IOException {
        Mockito.when(encoder.encodeCirrusBuildDoc(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenAnswer(
                        (args) -> {
                            final Row row = Row.withNames();
                            row.setField(
                                    UpdateFields.REV_ID,
                                    args.getArgument(0, JsonNode.class).at("/cirrusbuilddoc/version").asLong());
                            return row;
                        });
        revisionFetcher =
                new RevisionFetcher(
                        Duration.ofMillis(100),
                        new DefaultRevisionFetcherUriBuilder(),
                        encoder,
                        ImmutableMap.of(DOMAIN, info.getHttpBaseUrl()));
    }

    @AfterEach
    void closeFetcher() throws IOException {
        if (revisionFetcher != null) {
            revisionFetcher.close();
        }
    }

    @Test
    void properResponses() {
        final long revId = 551141L;
        final InputEvent revBasedChange = createRevBasedChange(revId, DOMAIN);

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .willReturn(WireMock.aResponse().withBodyFile("revision.missing.json"))
                        .willSetStateTo("recover"));

        assertThatThrownBy(() -> get(revisionFetcher, revBasedChange))
                .isInstanceOf(ExecutionException.class)
                .cause()
                .isInstanceOf(RevisionNotFoundException.class);

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("recover")
                        .willReturn(WireMock.aResponse().withBodyFile("revision." + revId + ".json")));

        assertThatNoException()
                .isThrownBy(
                        () -> {
                            assertThat(UpdateFields.getRevId(get(revisionFetcher, revBasedChange)))
                                    .isEqualTo(revId);
                        });
    }

    @Test
    void faultyResponses() {
        final long revId = 551142L;
        final InputEvent revisionCreateEvent = createRevBasedChange(revId, DOMAIN);

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .willReturn(WireMock.serverError().withFixedDelay(1000))
                        .willSetStateTo("too many requests"));

        assertThatThrownBy(() -> get(revisionFetcher, revisionCreateEvent))
                .isInstanceOf(ExecutionException.class)
                .cause()
                .isInstanceOf(IOException.class);

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("too many requests")
                        .willReturn(
                                WireMock.status(HttpStatus.SC_TOO_MANY_REQUESTS)
                                        .withHeader(HttpHeaders.RETRY_AFTER, "2")
                                        .withFixedDelay(50))
                        .willSetStateTo("server error"));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("server error")
                        .willReturn(WireMock.serverError().withFixedDelay(50))
                        .willSetStateTo("connection reset"));

        assertThatThrownBy(() -> get(revisionFetcher, revisionCreateEvent))
                .isInstanceOf(ExecutionException.class)
                .cause()
                .isInstanceOf(InvalidMWApiResponseException.class);

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("connection reset")
                        .willReturn(WireMock.serverError().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        assertThatThrownBy(() -> get(revisionFetcher, revisionCreateEvent))
                .isInstanceOf(ExecutionException.class)
                .cause()
                .isInstanceOf(IOException.class);
    }

    private static InputEvent createRevBasedChange(long revId, String domain) {
        InputEvent event = new InputEvent();
        event.setChangeType(ChangeType.REV_BASED_UPDATE);
        event.setEventTime(Instant.now());
        event.setRevId(revId);
        event.setDomain(domain);
        return event;
    }

    private static Row get(RevisionFetcher revisionFetcher, InputEvent inputEvent)
            throws ExecutionException, InterruptedException, TimeoutException {
        return revisionFetcher.apply(inputEvent).toCompletableFuture().get(5, TimeUnit.SECONDS);
    }
}
