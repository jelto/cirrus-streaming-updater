package org.wikimedia.discovery.cirrus.updater.producer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.flink.test.junit5.MiniClusterExtension;
import org.apache.flink.types.Row;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema.Builder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.salesforce.kafka.test.junit5.SharedKafkaTestResource;

@ExtendWith({MiniClusterExtension.class})
@SuppressWarnings("checkstyle:classfanoutcomplexity")
class ProducerApplicationIT {

    @RegisterExtension
    public static final SharedKafkaTestResource SHARED_KAFKA_TEST_RESOURCE =
            new SharedKafkaTestResource();

    @RegisterExtension static WireMockExtension wireMockExtension = getWireMockExtension();

    public static final String REV_TIMESTAMP_FIELD = "rev_timestamp";

    static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    static final EventRowTypeInfo UPDATE_TYPE_INFO = EventDataStreamUtilities.buildUpdateTypeInfo();
    static final JsonRowDeserializationSchema DESERIALIZATION_SCHEMA =
            new Builder(UPDATE_TYPE_INFO).build();

    @Test
    @Timeout(30)
    void test(WireMockRuntimeInfo runtimeInfo) throws Exception {

        final Instant now = Instant.now();
        final Instant endWM = now.plusSeconds(10);

        final String pageChangeTopic = "eqiad.rc0.mediawiki.page_change";
        final String successTopic = "eqiad.cirrussearch.update_pipeline.update";
        final String failureTopic = "eqiad.cirrussearch.update_pipeline.fetch_failure";

        SHARED_KAFKA_TEST_RESOURCE.getKafkaTestUtils().createTopic(pageChangeTopic, 1, (short) 1);
        SHARED_KAFKA_TEST_RESOURCE.getKafkaTestUtils().createTopic(successTopic, 1, (short) 1);
        SHARED_KAFKA_TEST_RESOURCE.getKafkaTestUtils().createTopic(failureTopic, 1, (short) 1);

        final Function<JsonNode, JsonNode> jsonNodeJsonNodeFunction =
                allEventsBefore(now, Duration.ofSeconds(-1));

        List<Instant> inputMessageTimestamps =
                producePageChangeRecords(jsonNodeJsonNodeFunction, pageChangeTopic, endWM);

        wireMockExtension.stubFor(
                WireMock.get(
                                WireMock.urlEqualTo(
                                        "/w/api.php?action=cirrus-config-dump&prop=replicagroup%7Cnamespacemap&format=json&formatversion=2"))
                        .willReturn(WireMock.aResponse().withBodyFile("testwiki-namespace-map.json")));
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551141.*"))
                        .willReturn(WireMock.aResponse().withBodyFile("revision.551141.json")));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551144.*"))
                        .inScenario("551144")
                        .willReturn(WireMock.aResponse().withBodyFile("revision.missing.json"))
                        .willSetStateTo("recover"));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551144.*"))
                        .inScenario("551144")
                        .whenScenarioStateIs("recover")
                        .willReturn(WireMock.aResponse().withBodyFile("revision.551144.json")));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551147.*"))
                        .inScenario("551147")
                        .willReturn(WireMock.serverError().withFixedDelay(1500))
                        .willSetStateTo("server error"));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551147.*"))
                        .inScenario("551147")
                        .whenScenarioStateIs("server error")
                        .willReturn(WireMock.serverError().withFixedDelay(500))
                        .willSetStateTo("connection reset"));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551147.*"))
                        .inScenario("551147")
                        .whenScenarioStateIs("connection reset")
                        .willReturn(WireMock.serverError().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        ProducerApplication.main(
                new String[] {
                    "--event-stream-config-url",
                    ProducerApplicationIT.class.getResource("/event-stream-config.json").toString(),
                    "--event-stream-json-schema-urls",
                    ProducerApplicationIT.class.getResource("/schema_repo").toString(),
                    "--page-change-stream",
                    "rc0.mediawiki.page_change",
                    "--kafka-source-config.bootstrap.servers",
                    SHARED_KAFKA_TEST_RESOURCE.getKafkaConnectString(),
                    "--kafka-source-config.group.id",
                    "test",
                    "--fetch-request-timeout",
                    "1s",
                    "--kafka-source-start-time",
                    inputMessageTimestamps.stream().min(Instant::compareTo).map(Instant::toString).get(),
                    "--kafka-source-end-time",
                    inputMessageTimestamps.stream().max(Instant::compareTo).map(Instant::toString).get(),
                    "--fetch-retry-max-age",
                    "15s",
                    "--http-routes.test.wikipedia.org",
                    runtimeInfo.getHttpBaseUrl(),
                    "--pipeline",
                    "integration_pipeline",
                    "--fetch-failure-topic",
                    failureTopic,
                    "--fetch-success-topic",
                    successTopic,
                });

        final List<ConsumerRecord<byte[], byte[]>> consumerRecords =
                SHARED_KAFKA_TEST_RESOURCE.getKafkaTestUtils().consumeAllRecordsFromTopic(successTopic);

        final List<ConsumerRecord<byte[], byte[]>> failedRecords =
                SHARED_KAFKA_TEST_RESOURCE.getKafkaTestUtils().consumeAllRecordsFromTopic(failureTopic);

        final List<Row> events =
                consumerRecords.stream()
                        .map(
                                event -> {
                                    try {
                                        return DESERIALIZATION_SCHEMA.deserialize(event.value());
                                    } catch (IOException e) {
                                        throw new RuntimeException(e);
                                    }
                                })
                        .collect(Collectors.toList());

        assertThat(events).hasSize(3);
        assertThat(
                        events.stream()
                                .filter(row -> UpdateFields.getOperation(row).equals(UpdateFields.OPERATION_DELETE))
                                .findFirst())
                .map(UpdateFields::getIndexName)
                .hasValue("testwiki_general");
        assertThat(failedRecords).hasSize(1);
    }

    private List<Instant> producePageChangeRecords(
            Function<JsonNode, JsonNode> lineMapper, String topic, Instant endWM)
            throws IOException, ExecutionException, InterruptedException {

        List<Future<RecordMetadata>> eventSends;
        try (Producer<Long, String> producer =
                SHARED_KAFKA_TEST_RESOURCE
                        .getKafkaTestUtils()
                        .getKafkaProducer(LongSerializer.class, StringSerializer.class)) {

            final Spliterator<JsonNode> eventNodeSpliterator =
                    Spliterators.spliteratorUnknownSize(
                            EventDataStreamUtilities.parseJson("/page_change.events.json").iterator(),
                            Spliterator.ORDERED);

            final Stream<JsonNode> eventNodes = StreamSupport.stream(eventNodeSpliterator, false);

            eventSends =
                    eventNodes
                            .map(lineMapper)
                            .map(line -> toProducerRecord(topic, line))
                            .map(producer::send)
                            .collect(Collectors.toList());
            // Send a fake event at time endWM (this is necessary to position the end offsets when using
            // bounded kafka streams)
            producer.send(new ProducerRecord<>(topic, 0, endWM.toEpochMilli(), null, ""));
        }
        List<Instant> timestamps = new ArrayList<>();
        for (Future<RecordMetadata> f : eventSends) {
            timestamps.add(Instant.ofEpochMilli(f.get().timestamp()));
        }
        timestamps.add(endWM);
        return timestamps;
    }

    @NotNull
    private static ProducerRecord<Long, String> toProducerRecord(String topic, JsonNode line) {
        return new ProducerRecord<>(
                topic,
                0,
                Instant.parse(line.get(REV_TIMESTAMP_FIELD).textValue()).toEpochMilli(),
                null,
                line.toString());
    }

    UnaryOperator<JsonNode> allEventsBefore(Instant now, Duration offset) {
        return (JsonNode event) -> {
            ((ObjectNode) event).put(REV_TIMESTAMP_FIELD, now.plus(offset).toString());
            return event;
        };
    }
}
